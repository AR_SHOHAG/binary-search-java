import java.util.Scanner;

public class BinarySearch {

	public int binary_search(int a[], int n, int key)
	{
		int i, start, end, mid;

		start  = 0;
		end   = n - 1;
	    mid = (start + end)/2;

	    while( start <= end && a[mid] != key )
	    {
	      if ( a[mid] > key )
	    	  end = mid - 1;
	      
	      else
	    	 start = mid + 1;
	      
	      mid = (start + end)/2;
	    
	   }
	   if ( start > end )
		    return -1;
	   
	   		return mid;
	      
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int j, n_m, key_m, a[];

	    Scanner in = new Scanner(System.in);
	    System.out.println("Enter the number of elements of array");
	    n_m = in.nextInt();
	    a = new int[n_m];

	    System.out.println("Enter " + n_m + " integers");


	    for (j = 0; j < n_m; j++)
	      a[j] = in.nextInt();

	    System.out.println("Enter the key elements");
	    key_m = in.nextInt();
	    
	    BinarySearch b = new BinarySearch();
	    
	    int pos=b.binary_search(a, n_m, key_m);
	    
	    if(pos >= 0){
	    	System.out.println(key_m + " is present at position " + (pos + 1) + ".");
	    }
	    
	    else
	    	System.out.println(key_m + " is not present in the array.");
	}

}
